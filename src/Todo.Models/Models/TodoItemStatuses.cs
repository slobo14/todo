﻿using System.ComponentModel.DataAnnotations;

namespace Todo.Models
{
    public enum TodoItemStatuses
    {
        [Display(Name = "To do")]
        ToDo = 1,
        [Display(Name = "In progress")]
        InProgress = 2,
        [Display(Name = "Done")]
        Done = 4
    }
}
