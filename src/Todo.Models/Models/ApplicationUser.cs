﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Todo.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
