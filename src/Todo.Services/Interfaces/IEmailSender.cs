﻿using System.Threading.Tasks;

namespace Todo.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
