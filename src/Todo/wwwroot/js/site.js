﻿var Completed = 4;

function onSuccessAdd(result) {
    toastr.success("Added successfully");
    $('#todoItem')[0].reset();
    if (result.status === Completed) return;
    $.ajax({
        url: '/Home/GetTodoItemList',
        type: "GET",
        dataType: "html",
        cache: false,
        success: function (data) {
            $("#todoList").html(data);
        },
        error: function (xhr, status, error) {
            toastr.error("Error while updating list!");
        }
    });
}

function onFailAdd(result) {
    toastr.error("Failed to add!");
}

function onSuccessSave(result) {
    toastr.success("Updated successfully");
    $.ajax({
        url: '/Admin/GetTodoItemListUpdate',
        type: "GET",
        dataType: "html",
        cache: false,
        success: function (data) {
            $("#mainBody").html(data);
        },
        error: function (xhr, status, error) {
            toastr.error("Error while loading list!");
        }
    });
}

function onFailSave(result) {
    if (result.status === 500) { //iz nekog razloga mi vrati error 500 iako na serveru u debuggeru piše 200 ok
        onSuccessSave(result);
        return;
    }
    toastr.error("Failed to save!");
}

function onSuccessDelete(result) {
    toastr.success("Deleted successfully");
    $("#id-" + result.todoItemID).remove();
    $("#count").text(parseInt($("#count").text()) - 1);
}

function onFailDelete(result) {
    toastr.error("Failed to delete!");
}

function deleteTodo(id) {
    $.ajax({
        url: '/todo/' + id,
        type: "DELETE",
        success: onSuccessDelete,
        error: onFailDelete
    });
}