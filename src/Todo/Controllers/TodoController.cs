using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Todo.Models;
using Todo.Interfaces;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Todo.Models.TodoViewModels;

namespace Todo.Controllers
{
    [Produces("application/json")]
    [Route("todo")]
    public class TodoController : Controller
    {
        private readonly ITodoRepository _todoRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ClaimsPrincipal _caller;

        public TodoController(ITodoRepository todoRepository, UserManager<ApplicationUser> userManager, ClaimsPrincipal caller)
        {
            _todoRepository = todoRepository;
            _userManager = userManager;
            _caller = caller;
        }

        [HttpGet]
        public IEnumerable<TodoItem> GetTodoItems()
        {
            return _todoRepository.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodoItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TodoItem todoItem = await _todoRepository.Find(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return Ok(todoItem);
        }

        //Prepravio sam na post, po�to put ne�e da radi preko forme
        [HttpPost("{id}")]
        public IActionResult PutTodoItem([FromRoute] int id, TodoItemViewModel todoItemVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todoItem = new TodoItem()
            {
                TodoItemID = todoItemVM.TodoItemID,
                Name = todoItemVM.Name,
                Description = todoItemVM.Description,
                Status = todoItemVM.Status,
                DateStart = todoItemVM.DateStart,
                DateEnd = todoItemVM.DateEnd
            };

            if (id != todoItem.TodoItemID)
            {
                return BadRequest();
            }

            try
            {
                _todoRepository.Update(todoItem);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> PostTodoItem(TodoItemViewModel todoItemVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todoItem = new TodoItem()
            {
                Name = todoItemVM.Name,
                Description = todoItemVM.Description,
                Status = todoItemVM.Status,
                DateStart = todoItemVM.DateStart,
                DateEnd = todoItemVM.DateEnd
            };

            try
            {
                var user = await GetCurrentUserAsync();
                todoItem.Owner = user;
                _todoRepository.Add(todoItem);
            }
            catch (DbUpdateException)
            {
                if (TodoItemExists(todoItem.TodoItemID))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTodoItem", new { id = todoItem.TodoItemID }, todoItem);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TodoItem todoItem = await _todoRepository.Find(id);
            if (todoItem == null)
            {
                return NotFound();
            }

            _todoRepository.Remove(todoItem);

            return Ok(todoItem);
        }

        private bool TodoItemExists(int id)
        {
            return _todoRepository.ItemExists(id);
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }
    }
}