﻿using Microsoft.AspNetCore.Mvc;

namespace Todo.Controllers
{
    public class HomeController : Controller
    {
        [Route("")]
        [Route("home")]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult GetTodoItemList()
        {
            return ViewComponent("TodoItemList");
        }
    }
}
