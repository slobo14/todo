using Microsoft.AspNetCore.Mvc;

namespace Todo.Controllers
{
    public class AdminController : Controller
    {
        [Route("admin-home")]
        public IActionResult Index()
        {
            return View(); //RedirectToAction("Index", "Home");
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult GetTodoItemListAdd()
        {
            return ViewComponent("TodoItemListAdd", new { showAllItems = true, delete = true, update = false });
        }

        public IActionResult GetTodoItemListDelete()
        {
            return ViewComponent("TodoItemList", new { showAllItems = true, delete = true, update = false });
        }

        public IActionResult GetTodoItemListUpdate()
        {
            return ViewComponent("TodoItemList", new { showAllItems = true, delete = false, update = true });
        }

        public IActionResult GetTodoItemUpdate(int id)
        {
            ViewBag.Edit = true;
            return ViewComponent("TodoItemAdd", new { id = id });
        }
    }
}