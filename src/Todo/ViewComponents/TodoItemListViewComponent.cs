﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Interfaces;
using Todo.Models;
using Todo.Models.TodoViewModels;

namespace Todo.ViewComponents
{
    public class TodoItemListViewComponent : ViewComponent
    {
        private readonly ITodoRepository _todoRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public TodoItemListViewComponent(ITodoRepository todoRepository, UserManager<ApplicationUser> userManager)
        {
            _todoRepository = todoRepository;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool showAllItems, bool delete, bool update)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var todoItems = showAllItems ?
                _todoRepository.FindByUser(user) 
              : _todoRepository.FindByUserUnfinished(user);

            var todoItemsVM = from item in todoItems
                            select new TodoItemViewModel()
                            {
                                TodoItemID = item.TodoItemID,
                                Name = item.Name,
                                Status = item.Status,
                                Description = item.Description,
                                DateStart = item.DateStart,
                                DateEnd = item.DateEnd
                            };

            ViewBag.Delete = delete;
            ViewBag.Update = update;

            return View(todoItemsVM);
        }
    }
}
