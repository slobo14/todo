﻿using Microsoft.AspNetCore.Mvc;

namespace Todo.ViewComponents
{
    public class TodoItemListAddViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
