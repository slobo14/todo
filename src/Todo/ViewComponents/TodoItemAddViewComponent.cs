﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Todo.Interfaces;
using Todo.Models.TodoViewModels;

namespace Todo.ViewComponents
{
    public class TodoItemAddViewComponent : ViewComponent
    {
        private readonly ITodoRepository _todoRepository;

        public TodoItemAddViewComponent(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? id = null)
        {
            var todoItemVM = new TodoItemViewModel();
            if(id != null)
            {
                var todoItem = await _todoRepository.Find((int)id);

                todoItemVM.TodoItemID = todoItem.TodoItemID;
                todoItemVM.Name = todoItem.Name;
                todoItemVM.Description = todoItem.Description;
                todoItemVM.Status = todoItem.Status;
                todoItemVM.DateStart = todoItem.DateStart;
                todoItemVM.DateEnd = todoItem.DateEnd;
            }

            return View(todoItemVM);
        }
    }
}
