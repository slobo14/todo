﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Todo.Models.TodoViewModels
{
    public class TodoItemViewModel
    {
        public int TodoItemID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public TodoItemStatuses Status { get; set; } = TodoItemStatuses.ToDo;

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date start")]
        public DateTime DateStart { get; set; } = DateTime.Now;

        [DataType(DataType.Date)]
        [Display(Name = "Date end")]
        public DateTime DateEnd { get; set; } = DateTime.Now;
    }
}
