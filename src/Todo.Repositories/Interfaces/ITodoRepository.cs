﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Todo.Models;

namespace Todo.Interfaces
{
    public interface ITodoRepository
    {
        void Add(TodoItem item);
        void Remove(TodoItem item);
        void Update(TodoItem item);
        IEnumerable<TodoItem> GetAll();
        Task<TodoItem> Find(int TodoItemID);
        IEnumerable<TodoItem> FindByUser(ApplicationUser user);
        IEnumerable<TodoItem> FindByUserAndStatus(ApplicationUser user, TodoItemStatuses inProgress);
        IEnumerable<TodoItem> FindByUserUnfinished(ApplicationUser user);
        bool ItemExists(int id);
    }
}
