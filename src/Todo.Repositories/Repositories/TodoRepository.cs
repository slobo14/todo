﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Data;
using Todo.Interfaces;
using Todo.Models;

namespace Todo.Repositories
{
    public class TodoRepository : ITodoRepository
    {

        private ApplicationDbContext _dbContext;

        public TodoRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async void Add(TodoItem item)
        {
            _dbContext.TodoItems.Add(item);
            await _dbContext.SaveChangesAsync();
        }

        public async void Remove(TodoItem item)
        {
            _dbContext.TodoItems.Remove(item);
            await _dbContext.SaveChangesAsync();
        }

        public async void Update(TodoItem item)
        {
            _dbContext.Entry(item).State = EntityState.Modified;
            try
            {
                await _dbContext.SaveChangesAsync();
            } catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task<TodoItem> Find(int TodoItemID)
        {
            return await _dbContext.TodoItems.SingleOrDefaultAsync(m => m.TodoItemID == TodoItemID);
        }

        public IEnumerable<TodoItem> GetAll()
        {
            return _dbContext.TodoItems.ToList();
        }

        public IEnumerable<TodoItem> FindByUser(ApplicationUser user)
        {
            if (user == null)
                return new List<TodoItem>();
            return _dbContext.TodoItems.Where(item => item.Owner == user);
        }

        public IEnumerable<TodoItem> FindByUserAndStatus(ApplicationUser user, TodoItemStatuses todoItemStatuses)
        {
            if (user == null)
                return new List<TodoItem>();
            return _dbContext.TodoItems.Where(item => item.Owner == user && ((item.Status & todoItemStatuses) == item.Status));
        }

        public IEnumerable<TodoItem> FindByUserUnfinished(ApplicationUser user)
        {
            return FindByUserAndStatus(user, TodoItemStatuses.ToDo | TodoItemStatuses.InProgress);
        }

        public bool ItemExists(int id)
        {
            return _dbContext.TodoItems.Any(e => e.TodoItemID == id);
        }
    }
}
